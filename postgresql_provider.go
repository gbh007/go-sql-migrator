package migrator

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"
)

type postgreSQLProvider struct{}

func (p *postgreSQLProvider) ApplyInnerMigration(ctx context.Context, tx *sqlx.Tx) error {
	_, err := tx.ExecContext(ctx, techMigrationPostgreSQL)
	if err != nil {
		return err
	}

	return nil
}

func (p *postgreSQLProvider) GetAppliedMigration(ctx context.Context, tx *sqlx.Tx) ([]Migration, error) {
	migrations := make([]Migration, 0)

	err := tx.SelectContext(ctx, &migrations, `SELECT * FROM migrations ORDER BY id;`)
	if err != nil {
		return nil, err
	}

	return migrations, nil
}

func (p *postgreSQLProvider) ApplyMigration(
	ctx context.Context, tx *sqlx.Tx, id int, body, filename, hash string,
) error {
	_, err := tx.ExecContext(ctx, body)
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(
		ctx,
		`INSERT INTO migrations (id, filename, hash, applied) VALUES ($1, $2, $3, $4);`,
		id, filename, hash, time.Now().UTC(),
	)
	if err != nil {
		return err
	}

	return nil
}
