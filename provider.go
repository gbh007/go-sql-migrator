package migrator

import (
	"context"

	"github.com/jmoiron/sqlx"
)

// Стандартные провайдеры.
var (
	PostgreSQLProvider Provider = &postgreSQLProvider{}
	MySQLProvider      Provider = &simpleProvider{innerMigration: techMigrationMariaDB}
	ClickHouseProvider Provider = &simpleProvider{innerMigration: techMigrationClickHouse}
	Sqlite3Provider    Provider = &simpleProvider{innerMigration: techMigrationSqlite3}
)

type Provider interface {
	// ApplyInnerMigration - применяет внутренние миграции для работы самого мигратора.
	ApplyInnerMigration(ctx context.Context, tx *sqlx.Tx) error
	// GetAppliedMigration - возвращает примененные миграции, упорядоченные по номеру.
	GetAppliedMigration(ctx context.Context, tx *sqlx.Tx) ([]Migration, error)
	// ApplyMigration - применяет миграцию.
	ApplyMigration(ctx context.Context, tx *sqlx.Tx, id int, body, filename, hash string) error
}
