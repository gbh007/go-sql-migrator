# Go SQL migrator

Примитивный мигратор для пет проектов, с поддержкой `fs.FS`.

**Не рекомендуется** у использованию в коммерческих или крупных проектах.

**Не поддерживает** откат миграции (downgrade).

## Поддерживаемые БД

1. PostgreSQL:
   - `PostgreSQLProvider` - схема по умолчанию, таблица `migrations`
2. Sqlite3
   - `Sqlite3Provider` - таблица `migrations`
3. MariaDB/MySQL
   - `MySQLProvider` - таблица `migrations`
4. ClickHouse
   - `ClickHouseProvider` - таблица `migrations`, движок `MergeTree`

## Примеры

Работает с файлами в корневой папке, вида `0000_name.sql`, 4 цифры и расширение `sql` обязательно.

Типовое использование

```go

import (
    migrator "gitlab.com/gbh007/go-sql-migrator"
)

// MigrateAll - производит миграции данных
func (storage *Database) MigrateAll(ctx context.Context) error {
    return migrator.New().
        WithFS(migration.Migrations).
        WithLogger(system.NewLogger(ctx)).
        WithProvider(migrator.Sqlite3Provider).
        MigrateAll(ctx, storage.db, true)
}
```

Встраивание

```go
//go:embed *.sql
var Migrations embed.FS
```

## TODO

1. Отказаться от использования `sqlx`, по причине упрощения зависимостей
2. Добавить провайдер для PostgreSQL с поддержкой кастомной схемы
