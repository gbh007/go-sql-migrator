package migrator

import (
	"context"
	"errors"
	"log"
)

var (
	// Ошибка миграций БД
	ErrMigrator = errors.New("migrator")
	// Пустой провайдер
	ErrNilProvider = errors.New("nil provider")

	// Невалидная конфигурация для сборки
	ErrInvalidBuildConfiguration = errors.New("invalid build configuration")
)

type Logger interface {
	Error(err error)
	Info(message string)
}

type CtxLogger interface {
	Error(ctx context.Context, err error)
	Info(ctx context.Context, message string)
}

type simpleLogger struct{}

func (*simpleLogger) Error(err error) {
	if err != nil {
		log.Println(err)
	}
}

func (*simpleLogger) Info(message string) {
	log.Println(message)
}

type ctxWrapper struct {
	logger Logger
}

func (l *ctxWrapper) Error(_ context.Context, err error) {
	if l.logger == nil {
		return
	}

	if err != nil {
		l.logger.Error(err)
	}
}

func (l *ctxWrapper) Info(_ context.Context, message string) {
	if l.logger == nil {
		return
	}

	l.logger.Info(message)
}
